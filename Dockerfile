#Dockerfile
FROM node:22-bookworm
LABEL Developers="Joey Chua"
WORKDIR /app
COPY . .
RUN npm ci
RUN sed -i "s/import.*prisma\/client';/import \{ createRequire \} from 'module';\nconst require = createRequire(import.meta.url ?? __filename);\nconst \{ Prisma \} = require('@prisma\/client');/" src/lib/zod/prisma/index.ts 
RUN npm run build
RUN rm -rf src/ static/ prisma/
USER node:node
CMD ["node","build/index.js"]