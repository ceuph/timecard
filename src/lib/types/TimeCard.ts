
/**
 * Represents a partial TimeCard detail object, containing various status and remark fields, 
 * as well as calculated values for unfulfilled time, over-break time, punch break time, 
 * and final calculated values for unfulfilled, over-break, and absence.
 */
export type TimeCardDetailPartial = {
    userStatus: string,
    userRemarks: string,
    headStatus?: string | null,
    headRemarks?: string | null,
    hrdStatus?: string | null,
    hrdRemarks?: string | null,
    hrdOverride: boolean,
    unfulfilled: number, // includes break time and grace period (seconds unit)
    overBreak: number, // includes grace period (seconds unit)
    punchBreak: number,
    unfulfilledFinal: number, // deducted break time and grace period (seconds unit)
    overBreakFinal: number, // deducted grace period (seconds unit)
    overtimeFinal: number, // overtime (seconds unit)
    absenceFinal: number,  // absence (days unit)
    unfulfilledFinalHrd: number, // deducted break time and grace period w/ hrd override (seconds unit)
    overBreakFinalHrd: number, // deducted grace period and grace period w/ hrd override (seconds unit)
    overtimeFinalHrd: number, // overtime w/ hrd override (seconds unit)
    absenceFinalHrd: number, // absence w/ hrd override (days unit)
};

/**
 * List of timecard statuses.
 */
export enum TimeCardStatus {
    Draft = 'DRAFT',
    EmployeeSubmitted = 'EMPLOYEE_SUBMITTED',
    ManagerSubmitted = 'MANAGER_SUBMITTED',
    HRDSubmitted = 'HRD_SUBMITTED',
}

/**
 * List of user statuses in Timecard Details.
 */
export enum TimeCardUserStatus {
    Accepted = 'ACCEPTED',
    Disputed = 'DISPUTED',
}

/**
 * List of head statuses in Timecard Details.
 */
export enum TimeCardHeadStatus {
    Approved = 'APPROVED',
    Disapproved = 'DISAPPROVED'
}

/**
 * List of HRD statuses in Timecard Details.
 */
export enum TimeCardHrdStatus {
    Accepted = 'ACCEPTED',
    Override = 'OVERRIDE'
}

export enum CutoffStatus {
    Active = 'Active',
    Inactive = 'Inactive'
}