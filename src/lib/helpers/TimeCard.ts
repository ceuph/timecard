import { TimeCardStatus, type TimeCardDetailPartial } from "$lib/types/TimeCard";
import type { DataTableRow } from "carbon-components-svelte/src/DataTable/DataTable.svelte";

export const toLocaleDateString = (date: Date) => {
    return date.toLocaleDateString('en-PH', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    });
};

export const getTimeCardStepFromStatusId = (statusId: string) => {
    let step = 0;
    if (statusId === TimeCardStatus.EmployeeSubmitted) step = 1;
    if (statusId === TimeCardStatus.ManagerSubmitted) step = 2;
    if (statusId === TimeCardStatus.HRDSubmitted) step = 3;
    return step;
}

export const getOverrideValue = (timecardDtl: TimeCardDetailPartial | DataTableRow, column: keyof TimeCardDetailPartial): number => {
    let value = timecardDtl[column];
    if (typeof value !== 'number') throw new Error(`Invalid column: ${column}`);
    if (timecardDtl.hrdOverride) {
        if (column === 'unfulfilledFinal') value = timecardDtl.unfulfilledFinalHrd;
        if (column === 'overBreakFinal') value = timecardDtl.overBreakFinalHrd;
        if (column === 'overtimeFinal') value = timecardDtl.overtimeFinalHrd;
        if (column === 'absenceFinal') value = timecardDtl.absenceFinalHrd;
    }
    return value;
}

export const getSecondsToHour = (seconds: number) => {
    return Math.floor((seconds * 1000) / 60 / 60) / 1000
}

export const getTimeCardSteps = () : {step: number, label: string, description?: string}[] => {
    return [
        {
            step: 1,
            label: 'Employee',
        },
        {
            step: 2,
            label: 'Manager',
        },
        {
            step: 3,
            label: 'HRD',
        }
    ]
}