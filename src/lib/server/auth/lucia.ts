import { Lucia, type Adapter } from 'lucia';

export const createLucia = (adapter: Adapter, dev: any) => {
	return new Lucia(adapter, {
		sessionCookie: {
			attributes: {
				// set to `true` when using HTTPS
				secure: !dev
			}
		},
		getUserAttributes: (attributes) => {
			return {
				id: attributes.id,
				username: attributes.username,
				picture: attributes.picture
			};
		}
	});
};

declare module 'lucia' {
	interface Register {
		Lucia: ReturnType<typeof createLucia>;
		DatabaseUserAttributes: DatabaseUserAttributes;
	}
}

interface DatabaseUserAttributes {
	id: string;
	username: string;
	picture: string;
}
