import { Keycloak, type KeycloakTokens } from 'arctic';
import { env } from '$env/dynamic/private';

const { KEYCLOAK_REALM_URL, KEYCLOAK_CLIENT_ID, KEYCLOAK_CLIENT_SECRET } = env;
export const createKeycloakAuth = (url: URL) => {
	const redirectURL = new URL(url.origin);
	redirectURL.pathname = '/login/keycloak/callback';
	return new Keycloak(
		KEYCLOAK_REALM_URL,
		KEYCLOAK_CLIENT_ID,
		KEYCLOAK_CLIENT_SECRET,
		redirectURL.toString()
	);
};

export const fetchKeycloakProfile = async (tokens: KeycloakTokens) => {
	return await fetch(`${KEYCLOAK_REALM_URL}/protocol/openid-connect/userinfo`, {
		headers: {
			Authorization: `Bearer ${tokens.accessToken}`
		}
	});
};
