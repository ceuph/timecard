import type { Handle } from "@sveltejs/kit";
import nodemailer from "nodemailer";
import { env } from "$env/dynamic/private";

export const handle = (async ({ event, resolve }) => {
    event.locals.mail = nodemailer.createTransport({
        host: env.MAIL_HOST,
        port: parseInt(env.MAIL_PORT),
        secure: true, // upgrade later with STARTTLS
        auth: {
            user: env.MAIL_KEY,
            pass: env.MAIL_SECRET,
        },
    });
    const response = await resolve(event);
    return response;
}) satisfies Handle;

export default handle;