import { dev } from '$app/environment';
import type { Handle } from '@sveltejs/kit';
import { PrismaAdapter } from '@lucia-auth/adapter-prisma';
import { createLucia } from '../auth/lucia';

let lucia: ReturnType<typeof createLucia>;

export const handle = (async ({ event, resolve }) => {
	const adapter = new PrismaAdapter(event.locals.prisma.session, event.locals.prisma.user);
	if (!lucia) lucia = createLucia(adapter, dev);
	event.locals.lucia = lucia;
	const sessionId = event.cookies.get(lucia.sessionCookieName);
	if (!sessionId) {
		event.locals.user = null;
		event.locals.session = null;
		return resolve(event);
	}

	const { session, user } = await lucia.validateSession(sessionId);
	if (session && session.fresh) {
		const sessionCookie = lucia.createSessionCookie(session.id);
		// sveltekit types deviates from the de-facto standard
		// you can use 'as any' too
		event.cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '.',
			...sessionCookie.attributes
		});
	}
	if (!session) {
		const sessionCookie = lucia.createBlankSessionCookie();
		event.cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '.',
			...sessionCookie.attributes
		});
	}
	event.locals.user = user;
	event.locals.session = session;
	return await resolve(event);
}) satisfies Handle;

export default handle;
