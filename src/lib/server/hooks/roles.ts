import type { Handle } from "@sveltejs/kit";

export const handle = (async ({ event, resolve }) => {
    event.locals.roles = new Set<string>();
    if (!event.locals.user) return await resolve(event);
    const roles = await event.locals.prisma.userRole.findMany({
        where: {
            userId: event.locals.user.id
        },
        include: {
            role: true
        }
    });
    event.locals.roles = new Set<string>(roles.map(r => r.role.name));
    const response = await resolve(event);
    return response;
}) satisfies Handle;

export default handle;