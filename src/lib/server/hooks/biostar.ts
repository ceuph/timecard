import type { Handle } from "@sveltejs/kit";
import BiostarClient from "../biostar/BiostarClient";
import { env } from "$env/dynamic/private";

let biostar: BiostarClient;

export const handle = (async ({ event, resolve }) => {
    const { BIOSTAR_API, BIOSTAR_TNA_API, BIOSTAR_USERNAME, BIOSTAR_PASSWORD } = env;
    if (!biostar) biostar = new BiostarClient(new URL(BIOSTAR_API), new URL(BIOSTAR_TNA_API), BIOSTAR_USERNAME, BIOSTAR_PASSWORD, event.fetch);
    event.locals.biostar = biostar;
    const response = await resolve(event);
    return response;
}) satisfies Handle;

export default handle;