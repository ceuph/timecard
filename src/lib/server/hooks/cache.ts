import type { Handle } from "@sveltejs/kit";
import NodeCache from "node-cache";

let cache: NodeCache;

export const handle = (async ({ event, resolve }) => {
    if (!cache) cache = new NodeCache({ stdTTL: 120, checkperiod: 600 });
    event.locals.cache = cache;
    const response = await resolve(event);
    return response;
}) satisfies Handle;

export default handle;