
// Prisma workaround https://github.com/prisma/prisma/issues/5030#issuecomment-1398076317
import type { PrismaClient as PrismaClientType } from '@prisma/client';
import { createRequire } from 'module';
import type { Handle } from '@sveltejs/kit';

const require = createRequire(import.meta.url ?? __filename);
const { PrismaClient: PrismaClientImpl } = require('@prisma/client');
export class PrismaClient extends (PrismaClientImpl as typeof PrismaClientType) {}

let prisma: PrismaClient;

export const handle = (async ({ event, resolve }) => {
	if (!prisma) prisma = new PrismaClient();
	event.locals.prisma = prisma;
	return await resolve(event);
}) satisfies Handle;

export default handle;
