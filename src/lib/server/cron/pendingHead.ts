// TODO test if TEST_MAIL is honored
// TODO test actual sending of email

import { env } from "$env/dynamic/private";
import { CutoffStatus, TimeCardStatus } from "$lib/types/TimeCard";

export const pendingHead = async (locals: App.Locals) => {
    const cutoffIds = await locals.prisma.cutoff.findMany({
        where: {
            status: CutoffStatus.Active
        },
    });

    for(const cutoff of cutoffIds) {
        const pendingTimeCards = await locals.prisma.timeCardValidation.groupBy({
            by: [ "headId", "headName" ],
            where: {
                cutoffId: cutoff.id,
                statusId: TimeCardStatus.EmployeeSubmitted
            },
            _count: {
                _all: true
            },
        });
        for (const pendingTimeCard of pendingTimeCards) {
            if (env.TEST_MAIL) {
                if (env.TEST_MAIL === pendingTimeCard.headId) sendEmail(locals);
                continue;
            }
            sendEmail(locals);
        }
    }

}

export const sendEmail = async (locals: App.Locals) => {

}

export default pendingHead;