import { TimeCardStatus, type TimeCardDetailPartial } from '$lib/types/TimeCard';
import { Prisma } from '@prisma/client';
import moment from "moment";
import { type BiostarSummary, type BiostarTimecards, type BiostarUser } from '../biostar/BiostarClient';
import { env } from '$env/dynamic/private';

const { GRACE_PERIOD_SEC, BREAK_PERIOD_SEC } = env;

export type TimeCardWithID = ReturnType<typeof getTimeCardWithID>;

/**
 * Get cuffoff by id.
 * @param id cutoff primary key
 * @param locals App.Locals
 * @returns Promise<Prisma.CutoffGetPayload<null>>
 */
export const getCutoffById = async (id: string, locals: App.Locals): Promise<Prisma.CutoffGetPayload<null> | null> => {
    const cutoff = await locals.prisma.cutoff.findUnique({
        where: {
            id
        }
    });
    return cutoff;
}

/**
 * Get timecard with id. Retrieve timecard from Biostar summary payload
 * and set time_card_date_string property as id.
 * @param summary 
 * @returns 
 */
export const getTimeCardWithID = (summary: BiostarSummary) => {
    const timecards: BiostarTimecards = summary.records[0].time_cards;

    let index = 0;
    return timecards.map((timecard) => {
        let unfulfilledFinal = timecard.unfulfilled_work_time - parseInt(BREAK_PERIOD_SEC) <= parseInt(GRACE_PERIOD_SEC) ? 0 : timecard.unfulfilled_work_time - parseInt(BREAK_PERIOD_SEC) - parseInt(GRACE_PERIOD_SEC);
        let overBreakFinal = timecard.break.over_break <= parseInt(GRACE_PERIOD_SEC) ? 0 : timecard.break.over_break - parseInt(GRACE_PERIOD_SEC);
        let absenceFinal = 0;
        let late_in = false;
        let early_out = false;
        let missing_break_start = false;
        let missing_break_end = false;
        for (const exception of timecard.exceptions) {
            if (exception.code === 2) {
                absenceFinal = 1;
                unfulfilledFinal = 0;
                overBreakFinal = 0;
                continue;
            }
            if (exception.code === 3) late_in = true;
            if (exception.code === 4) early_out = true;
            if (exception.code === 9) missing_break_start = true;
            if (exception.code === 10) missing_break_end = true;
        }
        if ((late_in && missing_break_start) || (early_out && missing_break_end)) {
            absenceFinal = 0.5;
            unfulfilledFinal = 0;
            overBreakFinal = 0;
            timecard.exceptions = [{ code: 100, name: 'Halfday' }];
        }
        return {
            ...timecard,
            id: timecard.time_card_date_string,
            idx: index++,
            unfulfilledFinal,
            overBreakFinal,
            absenceFinal,
        };
    });
}

export const getTimeCardAndDetailsByUserCutoff = async (locals: App.Locals, cutoffId: string, userId: string) => {
    return await locals.prisma.timeCardValidation.findFirst({
        where: {
            cutoffId: cutoffId,
            userId: userId
        },
        include: {
            timeCardValidationDetails: true
        }
    });
}
export const createTimeCardValidationDetailsMap = async (timeCardValidationDetails: Prisma.TimeCardValidationDetailGetPayload<null>[]) => {
    const detailMap = new Map<string, TimeCardDetailPartial>();
    timeCardValidationDetails.forEach((detail) => {
        const key = detail.timeCardDateString;
        if (!detailMap.has(key)) {
            detailMap.set(key, {
                userStatus: detail.userStatus,
                userRemarks: detail.userRemarks,
                headRemarks: detail.headRemarks,
                headStatus: detail.headStatus,
            });
        }
    });
    return detailMap;
}

/**
 * Get summary by cutoff.
 * @param cutoff 
 * @param locals 
 * @param bsUser 
 * @returns 
 */
export const getSummaryByCutoff = async (cutoffPayload: Prisma.CutoffGetPayload<null>, locals: App.Locals, bsUser: BiostarUser) => {
    const startDate = moment(cutoffPayload.startDate).format('YYYY-MM-DD');
    const endDate = moment(cutoffPayload.endDate).format('YYYY-MM-DD');
    const cacheKey = `${bsUser.user_id}-${startDate}-${endDate}`;
    const cache = locals.cache.get<BiostarSummary>(cacheKey);
    if (cache) {
        return cache;
    }
    const summary = await locals.biostar.getSummary([bsUser.user_id], startDate, endDate);
    locals.cache.set(cacheKey, summary);
    return summary;
}

/**
 * Get cutoffs with summary. Sumary contains the number of timecard submitted by employee, by manager, and by HRD.
 * @param locals App.Locals
 * @returns Array<Prisma.CutoffGetPayload<null>>
 */
export const getCutoffSummary = async (locals: App.Locals) => {
    const [employee, manager, hrd] = await Promise.all([
        locals.prisma.cutoff.findMany({
            orderBy: [
                { startDate: 'desc' },
                { endDate: 'desc' },
            ],
            select: {
                id: true,
                startDate: true,
                endDate: true,
                status: true,
                _count: {
                    select: { timeCardValidations: { where: { statusId: TimeCardStatus.EmployeeSubmitted } } }
                }
            }
        }),
        locals.prisma.cutoff.findMany({
            orderBy: [
                { startDate: 'desc' },
                { endDate: 'desc' },
            ],
            select: {
                id: true,
                startDate: true,
                endDate: true,
                status: true,
                _count: {
                    select: { timeCardValidations: { where: { statusId: TimeCardStatus.ManagerSubmitted } } }
                }
            }
        }),
        locals.prisma.cutoff.findMany({
            orderBy: [
                { startDate: 'desc' },
                { endDate: 'desc' },
            ],
            select: {
                id: true,
                startDate: true,
                endDate: true,
                status: true,
                _count: {
                    select: { timeCardValidations: { where: { statusId: TimeCardStatus.HRDSubmitted } } }
                }
            }
        })
    ]);

    const cutoffs = new Map(employee.map((cutoffs) => [cutoffs.id, { ...cutoffs, employeeSubmitted: cutoffs._count.timeCardValidations, managerSubmitted: 0, hrdSubmitted: 0 }]));
    for (const cutoff of manager) {
        const orig = cutoffs.get(cutoff.id);
        if (orig)
            cutoffs.set(cutoff.id, { ...orig, managerSubmitted: cutoff._count.timeCardValidations });
    }
    for (const cutoff of hrd) {
        const orig = cutoffs.get(cutoff.id);
        if (orig)
            cutoffs.set(cutoff.id, { ...orig, hrdSubmitted: cutoff._count.timeCardValidations });
    }

    return Array.from(cutoffs.values());
}