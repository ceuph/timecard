/**
 * Thrown when error occured in BiostarClient.
 */
export default class BiostarError extends Error {}