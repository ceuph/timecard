import BiostarError from "./BiostarError";

/**
 * Thrown when Authentication is incorrect.
 */
export default class BiostarAuthenticationError extends BiostarError {}