import BiostarError from "./BiostarError"

/**
 * Thrown when API returned an error response.
 */
export default class BiostarResponseError extends BiostarError {
    constructor(message: string, public status: number, public body: string | Record<string, string>) {
        super(message);
    }
};