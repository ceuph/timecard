import BiostarAuthenticationError from "./BiostarAuthenticationError";
import BiostarResponseError from "./BiostarResponseError";

type FetchAPI = typeof fetch;

/**
 * Type for the User returned by BioStar API.
 */
export type BiostarUser = {
	user_id: string;
	name: string;
	email: string;
}

/**
 * Type for the Summary returned by BioStar API.
 */
export type BiostarSummary = {
	records: Array<{
		user: { user_id: string, name: string, userGroup: { id: number, name: string } },
		time_cards: BiostarTimecards,
	}>,
	total: number,
}

/**
 * Type for the Exception returned by BioStar API.
 */
export type BiostarTimeCardException = {
	code: number;
	name: string;
};

/**
 * Type for an Array of Exceptions returned by BioStar API.
 */
export type BiostarTimeCardExceptions = Array<BiostarTimeCardException>;

/**
 * Type for the Timecard returnd by BioStar API.
 */
export type BiostarTimecard = {
	time_card_datetime: string;
	time_card_date_string: string;
	date_type: string;
	exceptions: BiostarTimeCardExceptions;
	leaves: Array<object>; // TODO determine structure of leaves
	holidays: Array<object>; // TODO determine structure of holidays
	shift: {
		name: string;
		type: string;
		day_start_time: number;
		fixed_option: {
			end_time: number;
			start_time: number;
		}
	},
	normal: {
		overtime: number,
	},
	in_time: string;
	in_time_string: string;
	is_in_time_next_day: boolean;
	out_time: string;
	out_time_string: string;
	regular_schedule_time: number;
	total_work_time: number;
	unfulfilled_work_time: number;
	time_rate: { regular: number, overtime: number };
	break: { over_break: number, punch_break: number };
};

/**
 * Type for an Array of Timecards returned by BioStar API.
 */
export type BiostarTimecards = Array<BiostarTimecard>;

/**
 * Acccess BioStart API through this client.
 */
export default class BiostarClient {
	private apiURL: URL;
	private tnaURL: URL;
	private username: string;
	private password: string;
	private fetch: FetchAPI;
	private bsSessionId: string | false = false;
	private bsSessionCookie: string | false = false;

	/**
	 * The constructor to set the client configuration.
	 * @param apiURL URL of BioStar API
	 * @param tnaURL URL of BioStar Time & Attendance API
	 * @param username BioStar User
	 * @param password BioStar Password
	 * @param fetch Fetch function to use
	 */
	public constructor(
		apiURL: URL,
		tnaURL: URL,
		username: string,
		password: string,
		fetch: FetchAPI
	) {
		this.apiURL = apiURL;
		this.tnaURL = tnaURL;
		this.username = username;
		this.password = password;
		this.fetch = fetch;
	}

	/**
	 * Login and get the session ID.
	 * @throws BiostarAuthenticationError
	 * @returns string BioStar Session ID
	 */
	public async login() {
		// TODO check if session expired before reusing
		// if (this.bsSessionId) return this.bsSessionId;
		const data = {
			User: {
				login_id: this.username,
				password: this.password
			}
		};
		const response = await this.fetch(`${this.apiURL}api/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});
		this.bsSessionId = response.headers.get('bs-session-id') ?? false;
		if (this.bsSessionId === false)
			throw new BiostarAuthenticationError('Invalid username or password');
		return this.bsSessionId;
	}

	/**
	 * Get required headers to access the API.
	 * @returns Object
	 */
	public async headers() {
		return {
			'Content-Type': 'application/json',
			'bs-session-id': await this.login()
		};
	}

	/**
	 * SSO login from current session and get TNA session cookie.
	 * @returns string session cookie
	 */
	public async tnaLogin() {
		const data = {
			user_id: this.username,
			biostar_session_id: await this.login()
		};
		const response = await this.fetch(`${this.tnaURL}tna/login/sso`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(data)
		});
		this.bsSessionCookie = response.headers.get('Set-Cookie') ?? false;
		if (this.bsSessionCookie === false)
			throw new BiostarAuthenticationError('Invalid TNA username or session id');
		return this.bsSessionCookie;
	}

	/**
	 * Get required headers to access the TNA API.
	 * @returns Object
	 */
	public async tnaHeaders() {
		return {
			'Content-Type': 'application/json',
			'bs-session-id': await this.login(),
			'Cookie': await this.tnaLogin()
		};
	}

	/**
	 * Retrieve user by email in the API.
	 * @param email 
	 * @returns 
	 */
	public async getUserByEmail(email: string): Promise<BiostarUser | null> {
		const data = {
			limit: 1,
			user_email: email,

		}
		const response = await this.fetch(`${this.apiURL}api/v2/users/advance_search`, {
			method: 'POST',
			headers: await this.headers(),
			body: JSON.stringify(data)
		});
		const json = await response.json();
		return json.UserCollection?.rows[0] ?? null;
	}

	/**
	 * Get the summary of user IDs based on a date range.
	 * @param userIdList Array<string> user IDs
	 * @param startDate string date in YYYY-MM-DD
	 * @param endDate string date in YYYY-MM-DD
	 * @returns BiostarSummary
	 */
	public async getSummary(userIdList: Array<string>, startDate: string, endDate: string): Promise<BiostarSummary> {
		const data = {
			start_date: startDate,
			end_date: endDate,
			user_id_list: userIdList,
			type: 'MONTHLY'
		}
		const response = await this.fetch(`${this.tnaURL}tna/time_cards/summary`, {
			method: 'POST',
			headers: await this.tnaHeaders(),
			body: JSON.stringify(data)
		});
		const jsonResponse = await response.json() as BiostarSummary;
		return jsonResponse;
	}

	/**
	 * Update a user's email in BioStar.
	 * @param userId 
	 * @param email 
	 * @returns 
	 */
	public async updateUserEmail(userId: string, email: string) {
		const data = {
			User: {
				email
			}
		};
		const response = await this.fetch(`${this.apiURL}api/users/${encodeURIComponent(userId)}`, {
			method: 'PUT',
			headers: await this.headers(),
			body: JSON.stringify(data)
		});
		const jsonResponse = await response.json();
		if (!response.ok) throw new BiostarResponseError(`Error: Failed to update User "${userId}" with email "${email}".`, response.status, jsonResponse);
		return { ...jsonResponse, userId, email };
	}
}
