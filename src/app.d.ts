// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
import type BiostarClient from '$lib/server/biostar/BiostarClient';
import type { PrismaClient } from '@prisma/client';
import type { Lucia, Session, User } from 'lucia';
import type NodeCache from 'node-cache';
import type { Transporter, SentMessageInfo, TransportOptions } from 'nodemailer';

declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			prisma: PrismaClient;
			lucia: Lucia;
			user: User | null;
			session: Session | null;
			biostar: BiostarClient;
			cache: NodeCache;
			roles: Set<string>;
			mail: Transporter<SentMessageInfo, TransportOptions>
		}
		interface PageData {
			flash?: { type: 'success' | 'error'; message: string };
		}
		// interface PageState {}
		// interface Platform {}
	}
}

export { };
