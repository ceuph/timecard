import { sequence } from '@sveltejs/kit/hooks';
import prisma from '$lib/server/hooks/prisma';
import lucia from '$lib/server/hooks/lucia';
import biostar from '$lib/server/hooks/biostar';
import cache from '$lib/server/hooks/cache';
import roles from '$lib/server/hooks/roles';
import mail from '$lib/server/hooks/mail';

export const handle = sequence(prisma, lucia, biostar, cache, roles, mail);
