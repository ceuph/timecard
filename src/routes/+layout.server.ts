import { loadFlash } from 'sveltekit-flash-message/server';
import type { LayoutServerLoad } from './$types';

export const load = loadFlash(async ({ locals, url }) => {
	let tempPath = '/';
	const crumbs = [{ path: tempPath, title: 'Home' }];
	if (url.pathname !== '/') url.pathname.split('/').slice(1).forEach(path => {
		if (path !== '') crumbs.push({
			path: tempPath += `${path}/`,
			title: `${path.substring(0, 1).toUpperCase()}${path.substring(1)}`
		});
	});
	return { user: locals.user, crumbs };
}) satisfies LayoutServerLoad;
