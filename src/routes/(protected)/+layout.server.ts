import { redirect } from '@sveltejs/kit';
import type { LayoutServerLoad } from './$types';

export const load = (async ({ locals }) => {
    if (locals.user === null) {
        redirect(303, '/');
    }
    return { user: locals.user };
}) satisfies LayoutServerLoad;