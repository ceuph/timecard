import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    const user = locals.user;
    if (!user) throw error(401, 'Unauthorized');
    const departments = await locals.prisma.department.findMany({
        where: {
            headId: user.username
        },
        include: { campus: true }
    })
    if (departments.length === 0) throw error(401, 'Unauthorized');
    return { departments, user };
}) satisfies PageServerLoad;