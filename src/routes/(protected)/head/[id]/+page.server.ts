import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { toLocaleDateString } from '$lib/helpers/TimeCard';

export const load = (async ({ locals, params, url, parent}) => {
    const { user } = await parent();
    const department = await locals.prisma.department.findFirst({
        where: {
            id: params.id
        },
        include: {
            employees: true,
            campus: true
        }
    });
    if (!department) throw error(404, 'Department not found');
    if (department.headId != user.username) throw error(403, 'You are not the head of this department');
    
    const cutoffs = await locals.prisma.cutoff.findMany({
        where: {
            status: 'Active'
        },
        orderBy: [
            { startDate: 'desc' }
        ]
    });
    const cutoffSet: Set<{id: string, text: string}> = new Set();
    for (const cutoff of cutoffs) {
        cutoffSet.add({id: cutoff.id, text: `${toLocaleDateString(cutoff.startDate)} - ${toLocaleDateString(cutoff.endDate)}`});
    }

    const selectedId = url.searchParams.get('cutoff') ?? '';
    const timecards = await locals.prisma.timeCardValidation.findMany({
        select: {
            id: true,
            displayName: true,
            statusId: true,
        },
        where: {
            cutoffId: selectedId,
            headId: user.username,
        },
        orderBy: [
            { displayName: 'asc' }
        ]
    });
    return { cutoffs: Array.from(cutoffSet), timecards, selectedId, department };
}) satisfies PageServerLoad;