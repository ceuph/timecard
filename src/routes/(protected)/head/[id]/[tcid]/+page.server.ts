import { error, fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import type { Actions } from "./$types";
import { Prisma } from '@prisma/client'
import { TimeCardHeadStatus, TimeCardStatus } from '$lib/types/TimeCard';
import type { User } from 'lucia';

let user: User;
let timecard: Prisma.TimeCardValidationGetPayload<{ include: { timeCardValidationDetails: true } }> | null;
let department: Prisma.DepartmentGetPayload<{ include: { head: true } }> | null;
const getPrevUrl = (id: string, url: URL) => {
    const prevUrl = new URL(`/head/${id}`, url);
    prevUrl.search = url.search;
    return prevUrl.toString();
}

export const load = (async ({ locals, params, parent }) => {
    user = (await parent()).user;
    timecard = await locals.prisma.timeCardValidation.findUnique({
        where: {
            id: params.tcid
        },
        include: {
            timeCardValidationDetails: {
                orderBy: {
                    timeCardDateString: 'asc'
                }
            }
        }
    });
    if (!timecard) throw error(404, 'Timecard not found.');

    department = await locals.prisma.department.findUnique({
        where: {
            id: params.id
        },
        include: {
            head: true
        }
    });
    if (!department) throw error(404, 'Department not found.');
    if (department.headId != user.username) throw error(403, 'You are not the head of this department.');

    return {
        timecard: {
            ...timecard,
            totalUnfulfilledFinalHrd: timecard.totalUnfulfilledFinalHrd?.toNumber(),
            totalOverBreakFinalHrd: timecard.totalOverBreakFinalHrd?.toNumber(),
            totalOvertimeFinalHrd: timecard.totalOvertimeFinalHrd?.toNumber(),
            totalAbsenceFinalHrd: timecard.totalAbsenceFinalHrd?.toNumber(),
            timeCardValidationDetails: timecard.timeCardValidationDetails.map((t) => ({
                ...t,
                overtimeFinal: t.overtimeFinal.toNumber(),
                overtimeFinalHrd: t.overtimeFinalHrd?.toNumber(),
                unfulfilled: t.unfulfilled.toNumber(),
                overBreak: t.overBreak.toNumber(),
                punchBreak: t.punchBreak.toNumber(),
                unfulfilledFinal: t.unfulfilledFinal.toNumber(),
                unfulfilledFinalHrd: t.unfulfilledFinalHrd?.toNumber(),
                overBreakFinal: t.overBreakFinal.toNumber(),
                overBreakFinalHrd: t.overBreakFinalHrd?.toNumber(),
                absenceFinal: t.absenceFinal.toNumber(),
                absenceFinalHrd: t.absenceFinalHrd?.toNumber(),
            }))
        }
    };
}) satisfies PageServerLoad;

export const actions = {
    default: async ({ request, locals, params, url }) => {
        if (!timecard) throw error(404, 'Timecard not found.');
        if (!department) throw error(404, 'Department not found.');

        const form = await request.formData();
        const errors: Map<string, string> = new Map();

        for (const row of timecard.timeCardValidationDetails) {
            await locals.prisma.timeCardValidationDetail.update({
                where: {
                    id: row.id,
                },
                data: {
                    headStatus: form.get(`disapprove[${row.id}]`) !== null ? TimeCardHeadStatus.Disapproved : TimeCardHeadStatus.Approved,
                    headRemarks: form.get(`remarks[${row.id}]`)?.toString() ?? null,
                }
            })
            if (form.get(`disapprove[${row.id}]`) !== null && !form.get(`remarks[${row.id}]`)) {
                errors.set(row.id, 'Remarks are required for Disapproved status.');
            }
        }
        if (errors.size) {
            return fail(400, { errors, data: new Map(form.entries()) });
        }
        await locals.prisma.timeCardValidation.update({
            where: {
                id: timecard.id,
            },
            data: {
                statusId: TimeCardStatus.ManagerSubmitted,
                headId: user.username,
                headName: department.head.name
            }
        });
        redirect(303, getPrevUrl(params.id, url));
    }
} satisfies Actions;