import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    if (!locals.roles.has('HRD')) throw error(403, 'You are not authorized to view this page');
    return {};
}) satisfies PageServerLoad;