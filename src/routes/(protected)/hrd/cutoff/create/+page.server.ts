import { fail, superValidate } from 'sveltekit-superforms';
import type { PageServerLoad } from './$types';
import { zod } from 'sveltekit-superforms/adapters';
import { CutoffOptionalDefaultsSchema as CutoffSchema } from '$lib/zod/prisma';
import type { Actions } from "./$types";
import { redirect } from 'sveltekit-flash-message/server';

export const load = (async () => {
    const form = await superValidate(zod(CutoffSchema));
    return { form };
}) satisfies PageServerLoad;


export const actions = {
    default: async ({ locals, request, cookies }) => {
        const form = await superValidate(request, zod(CutoffSchema));
        if (!form.valid) return fail(400, { form });
        await locals.prisma.cutoff.create({ data: form.data });
        return redirect('/hrd/cutoff', { type: 'success', message: 'Cuttoff saved successfully'}, cookies);
    }
} satisfies Actions;