import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { fail, superValidate } from 'sveltekit-superforms';
import { zod } from 'sveltekit-superforms/adapters';
import type { Actions } from "./$types";
import { CutoffOptionalDefaultsSchema as CutoffSchema } from '$lib/zod/prisma';
import { redirect } from 'sveltekit-flash-message/server';

export const load = (async ({ locals, params }) => {
    const cutoff = await locals.prisma.cutoff.findFirst({
        where: {
            id: params.id
        }
    });
    if (!cutoff) throw error(404, 'Cuttoff not found');
    const form = await superValidate(cutoff, zod(CutoffSchema))
    return { form };
}) satisfies PageServerLoad;


export const actions = {
    default: async ({ locals, request, params, cookies }) => {
        const form = await superValidate(request, zod(CutoffSchema));
        if (!form.valid) return fail(400, { form });
        await locals.prisma.cutoff.update({ where: { id: params.id }, data: { ...form.data, updatedAt: new Date() } });
        return redirect('/hrd/cutoff', { type: 'success', message: 'Cuttoff saved successfully'}, cookies);
    }
} satisfies Actions;