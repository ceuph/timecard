import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    const cutoffs = locals.prisma.cutoff.findMany({
        orderBy: [
            { startDate: 'desc' },
            { endDate: 'desc' },
        ],
    });
    return { cutoffs };
}) satisfies PageServerLoad;