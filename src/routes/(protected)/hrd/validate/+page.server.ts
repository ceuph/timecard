import { getCutoffSummary } from '$lib/server/helpers/TimeCard';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    const cutoffs = await getCutoffSummary(locals);
    return { cutoffs };
}) satisfies PageServerLoad;