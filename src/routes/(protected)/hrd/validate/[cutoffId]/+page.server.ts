import { TimeCardStatus } from '$lib/types/TimeCard';
import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals, params }) => {
    const cutoff = await locals.prisma.cutoff.findFirst({
        where: {
            id: params.cutoffId
        }
    });
    if (!cutoff) throw error(404, 'Cutoff not found');

    const departmentSummary = new Map<string, {
        id: string,
        department: string,
        campus: string,
        head: string,
        headId: string,
        employeeSubmittedCount: number,
        managerSubmittedCount: number,
        hrdSubmittedCount: number,
    }>();
    const departments = await locals.prisma.department.findMany({
        orderBy: { name: 'asc' },
        include: { head: true, employees: true, campus: true }
    });
    for (const department of departments) {
        const [employeeSubmittedCount, managerSubmittedCount, hrdSubmittedCount] = await Promise.all([
            locals.prisma.timeCardValidation.count({
                where: {
                    departmentId: department.id,
                    statusId: TimeCardStatus.EmployeeSubmitted,
                    cutoffId: params.cutoffId,
                }
            }),
            locals.prisma.timeCardValidation.count({
                where: {
                    departmentId: department.id,
                    statusId: TimeCardStatus.ManagerSubmitted,
                    cutoffId: params.cutoffId,
                }
            }),
            locals.prisma.timeCardValidation.count({
                where: {
                    departmentId: department.id,
                    statusId: TimeCardStatus.HRDSubmitted,
                    cutoffId: params.cutoffId,
                }
            }),
        ]);
        departmentSummary.set(department.id, {
            id: department.id,
            department: department.name,
            campus: department.campus.name,
            head: department.head.name,
            headId: department.head.id,
            employeeSubmittedCount,
            managerSubmittedCount,
            hrdSubmittedCount,
        });
    }
    return { cutoff, departments, departmentSummary: Array.from(departmentSummary.values()) };
}) satisfies PageServerLoad;