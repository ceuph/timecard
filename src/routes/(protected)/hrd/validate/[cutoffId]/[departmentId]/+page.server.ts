import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals, params }) => {
    const cutoff = await locals.prisma.cutoff.findFirst({
        where: {
            id: params.cutoffId,
        }
    });
    if (!cutoff) throw error(404, 'Cutoff not found');
    const department = await locals.prisma.department.findFirst({
        where: {
            id: params.departmentId,
        },
        include: { campus: true }
    });
    if (!department) throw error(404, 'Department not found');
    const timeCardValidation = await locals.prisma.timeCardValidation.findMany({
        select: {
            id: true,
            displayName: true,
            statusId: true,
        },
        where: {
            cutoffId: params.cutoffId,
            departmentId: params.departmentId,
        }
    });
    return { cutoff, department, timeCardValidation };
}) satisfies PageServerLoad;