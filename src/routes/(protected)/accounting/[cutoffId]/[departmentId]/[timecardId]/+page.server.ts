import { error, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import type { Actions } from "./$types";
import { Prisma } from '@prisma/client';
import { Decimal } from '@prisma/client/runtime/library';
import { TimeCardStatus } from '$lib/types/TimeCard';

let timecard: Prisma.TimeCardValidationGetPayload<{
    include: {
        timeCardValidationDetails: {
            orderBy: { timeCardDateString: 'asc' },
        },
    },
}> | null = null;
export const load = (async ({ locals, params }) => {
    timecard = await locals.prisma.timeCardValidation.findFirst({
        where: {
            id: params.timecardId
        },
        include: {
            timeCardValidationDetails: {
                orderBy: { timeCardDateString: 'asc' },
            },
        }
    });
    if (!timecard) throw error(404, 'Not found');
    const employee = await locals.prisma.employee.findFirst({
        where: {
            id: timecard.userId
        },
        include: {
            department: true,
        }
    });
    if (!employee) throw error(404, 'Not found');
    if (timecard.statusId === TimeCardStatus.HRDSubmitted) throw redirect(303, `/timecard/${params.timecardId}`);
    return { 
        employee,
        timecard: {
            ...timecard,
            totalUnfulfilledFinalHrd: timecard.totalUnfulfilledFinalHrd?.toNumber(),
            totalOverBreakFinalHrd: timecard.totalOverBreakFinalHrd?.toNumber(),
            totalOvertimeFinalHrd: timecard.totalOvertimeFinalHrd?.toNumber(),
            totalAbsenceFinalHrd: timecard.totalAbsenceFinalHrd?.toNumber(),
            timeCardValidationDetails: timecard.timeCardValidationDetails.map((t) => ({
                ...t,
                overtimeFinal: t.overtimeFinal.toNumber(),
                overtimeFinalHrd: t.overtimeFinalHrd?.toNumber(),
                unfulfilled: t.unfulfilled.toNumber(),
                overBreak: t.overBreak.toNumber(),
                punchBreak: t.punchBreak.toNumber(),
                unfulfilledFinal: t.unfulfilledFinal.toNumber(),
                unfulfilledFinalHrd: t.unfulfilledFinalHrd?.toNumber(),
                overBreakFinal: t.overBreakFinal.toNumber(),
                overBreakFinalHrd: t.overBreakFinalHrd?.toNumber(),
                absenceFinal: t.absenceFinal.toNumber(),
                absenceFinalHrd: t.absenceFinalHrd?.toNumber(),
            }))
        }
    };
}) satisfies PageServerLoad;

export const actions = {
    default: async ({ request, locals }) => {
        if (!timecard) throw error(404, 'Not found');
        const data = await request.formData();
        const dataMap = new Map(data.entries());
        const errors = new Map<string, string>();

        const rowPromises = [];
        let totalOvertimeFinalHrd = 0;
        let totalUnfulfilledFinalHrd = 0;
        let totalOverBreakFinalHrd = 0;
        let totalAbsenceFinalHrd = 0;
        for (const row of timecard.timeCardValidationDetails) {
            if (dataMap.has(`override[${row.id}]`)) {
                if(dataMap.has(`remarks[${row.id}]`) && dataMap.get(`remarks[${row.id}]`)?.toString() === '') {
                    errors.set(`remarks[${row.id}]`, 'Remarks is required');
                }
                row.hrdOverride = true;
                row.overtimeFinalHrd = new Decimal(dataMap.get(`overtimeFinal[${row.id}]`)?.toString() ?? '0');
                row.unfulfilledFinalHrd = new Decimal(dataMap.get(`unfulfilledFinal[${row.id}]`)?.toString() ?? '0');
                row.overBreakFinalHrd = new Decimal(dataMap.get(`overBreakFinal[${row.id}]`)?.toString() ?? '0');
                row.absenceFinalHrd = new Decimal(dataMap.get(`absenceFinal[${row.id}]`)?.toString() ?? '0');
            } else {
                row.hrdOverride = false;
                row.overtimeFinalHrd = row.overtimeFinal;
                row.unfulfilledFinalHrd = row.unfulfilledFinal;
                row.overBreakFinalHrd = row.overBreakFinal;
                row.absenceFinalHrd = row.absenceFinal;
            }
            row.hrdRemarks = dataMap.get(`remarks[${row.id}]`)?.toString() ?? '';
            rowPromises.push(locals.prisma.timeCardValidationDetail.update({
                where: {
                    id: row.id,
                },
                data: {
                    hrdOverride: row.hrdOverride,
                    overtimeFinalHrd: row.overtimeFinalHrd,
                    unfulfilledFinalHrd: row.unfulfilledFinalHrd,
                    overBreakFinalHrd: row.overBreakFinalHrd,
                    absenceFinalHrd: row.absenceFinalHrd,
                    hrdRemarks: row.hrdRemarks,
                }
            }));
            totalOvertimeFinalHrd += row.overtimeFinalHrd.toNumber();
            totalUnfulfilledFinalHrd += row.unfulfilledFinalHrd.toNumber();
            totalOverBreakFinalHrd += row.overBreakFinalHrd.toNumber();
            totalAbsenceFinalHrd += row.absenceFinalHrd.toNumber();
        }
        rowPromises.push(
            locals.prisma.timeCardValidation.update({
                where: {
                    id: timecard.id,
                },
                data: {
                    totalOvertimeFinalHrd: new Decimal(totalOvertimeFinalHrd),
                    totalUnfulfilledFinalHrd: new Decimal(totalUnfulfilledFinalHrd),
                    totalOverBreakFinalHrd: new Decimal(totalOverBreakFinalHrd),
                    totalAbsenceFinalHrd: new Decimal(totalAbsenceFinalHrd),
                    statusId: dataMap.get('action') === 'submit' ? TimeCardStatus.HRDSubmitted : undefined,
                }
            })
        );
        await Promise.all(rowPromises);
        return { data: dataMap, errors };
    }
} satisfies Actions;