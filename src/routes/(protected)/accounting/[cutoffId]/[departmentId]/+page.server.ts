import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { TimeCardStatus } from '$lib/types/TimeCard';

export const load = (async ({ locals, params }) => {
    const cutoff = await locals.prisma.cutoff.findFirst({
        where: {
            id: params.cutoffId,
        }
    });
    if (!cutoff) throw error(404, 'Cutoff not found');
    const department = await locals.prisma.department.findFirst({
        where: {
            id: params.departmentId,
        },
        include: { campus: true }
    });
    if (!department) throw error(404, 'Department not found');
    const timeCardValidation = await locals.prisma.timeCardValidation.findMany({
        select: {
            id: true,
            displayName: true,
            totalAbsenceFinalHrd: true,
            totalUnfulfilledFinalHrd: true,
            totalOverBreakFinalHrd: true,
            totalOvertimeFinalHrd: true,
            statusId: true,
        },
        where: {
            cutoffId: params.cutoffId,
            departmentId: params.departmentId,
            statusId: TimeCardStatus.HRDSubmitted
        },
        orderBy: {
            displayName: 'asc',
        }
    });
    return { cutoff, department, timeCardValidation: timeCardValidation.map(t => ({
        ...t,
        totalAbsenceFinalHrd: t.totalAbsenceFinalHrd?.toNumber() ?? 0,
        totalUnfulfilledFinalHrd: t.totalUnfulfilledFinalHrd?.toNumber() ?? 0,
        totalOverBreakFinalHrd: t.totalOverBreakFinalHrd?.toNumber() ?? 0,
        totalOvertimeFinalHrd: t.totalOvertimeFinalHrd?.toNumber() ?? 0,
    })) };
}) satisfies PageServerLoad;