import { env } from '$env/dynamic/private';
import type { PageServerLoad } from './$types';
import moment from 'moment-timezone';

const getSummary = async (user_id: string, locals: App.Locals, offset: number) => {
    const firstDay = `${moment.tz(env.TIMEZONE).add(offset, 'month').format('YYYY-MM')}-01`;
    const lastDay = moment.tz(firstDay, env.TIMEZONE).add(1, 'month').subtract(1, 'day').format('YYYY-MM-DD');
    const summary = await locals.biostar.getSummary([user_id], firstDay, lastDay);
    const weekly = getWeeklySummary(summary);
    return { summary, weekly };
};

const getWeeklySummary = (summary: Record<string | number | symbol, unknown>) => {
    const weeklySummary: Array<Array<Record<string | number | symbol, unknown>>> = [];
    if (summary.records instanceof Array) {
        for(const timecard of summary.records[0].time_cards) {
            const date = moment.tz(timecard.time_card_date_string, 'Asia/Manila');
            const week = date.week();
            const weekday = date.weekday();
            if (!weeklySummary[week]) weeklySummary[week] = [];
            weeklySummary[week][weekday] = timecard;
        }
    }
    return weeklySummary;
}

export const load = (async ({ locals, url, parent }) => {
    const { bsUser } = await parent();

    let offset = parseInt(url.searchParams.get('offset') ?? '0');
    if (isNaN(offset)) offset = 0;
    
    const summary = getSummary(bsUser.user_id, locals, offset);
    const subheader = moment.tz(env.TIMEZONE).add(offset, 'month').format('MMMM YYYY');
    return { bsUser, summary, subheader, offset };
}) satisfies PageServerLoad;