import type { PageServerLoad } from './$types'

export const load = (async ({ locals, parent }) => {
    const { bsUser } = await parent();
    const cutoffs = locals.prisma.cutoff.findMany({
        where: {
            status: 'Active'
        },
        orderBy: {
            startDate: 'desc'
        },
        include: {
            timeCardValidations: {
                select: {
                    statusId: true
                },
                where: {
                    userId: bsUser.user_id
                }
            },
        }
    });
    return { cutoffs };
}) satisfies PageServerLoad;