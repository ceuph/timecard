import { error, fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { Prisma } from '@prisma/client';
import type { Actions } from "./$types";
import type { BiostarUser } from '$lib/server/biostar/BiostarClient';
import { TimeCardStatus, TimeCardUserStatus } from '$lib/types/TimeCard';
import { getCutoffById, createTimeCardValidationDetailsMap, getTimeCardWithID, getSummaryByCutoff, getTimeCardAndDetailsByUserCutoff, type TimeCardWithID } from '$lib/server/helpers/TimeCard';

// const generateTimecardForm = async (timecard: Promise<any>) => {
//     const rows = await timecard;
//     let schema = z.object({});
//     rows.forEach((row: Record<string, any>) => {
//         schema = schema.extend({
//             [`dispute_${row.id.replaceAll('-', '')}`]: z.string().optional(),
//             [`remarks_${row.id.replaceAll('-', '')}`]: z.string().optional()
//         });
//     });
//     return superValidate(zod(schema));
// }

let bsUser: BiostarUser;
let cutoff: Prisma.CutoffGetPayload<null>;
let timecard: TimeCardWithID;
let employee: Prisma.EmployeeGetPayload<{ include: { department: { include: { head: true } } } }>;

export const load = (async ({ locals, params, parent }) => {
    bsUser = (await parent()).bsUser;

    const tmpEmployee = await locals.prisma.employee.findFirst({ where: { id: bsUser.user_id }, include: { department: { include: { head: true } } } });
    if (!tmpEmployee) {
        console.error('Employee not found', { bsUser });
        throw error(404, 'Employee not found. Please contact HRD.');
    }
    employee = tmpEmployee;
    if (!employee.department?.head?.id) throw error(404, 'Department head not found. Please contact HRD.');

    const timeCardValidation = await getTimeCardAndDetailsByUserCutoff(locals, params.id, bsUser.user_id);
    if (timeCardValidation && timeCardValidation.statusId !== TimeCardStatus.Draft) throw redirect(303, `/timecard/${timeCardValidation.id}`);
    const tcDetailMap = await createTimeCardValidationDetailsMap(timeCardValidation?.timeCardValidationDetails ?? []);
    const tmpCutoff = await getCutoffById(params.id, locals);
    if (!tmpCutoff) return redirect(303, '..');
    cutoff = tmpCutoff;
    const summary = await getSummaryByCutoff(cutoff, locals, bsUser);
    timecard = getTimeCardWithID(summary);
    return { employee, cutoff, timecard, tcDetailMap };
}) satisfies PageServerLoad;


export const actions = {
    default: async ({ request, locals, params }) => {
        const form = await request.formData();
        const errors: Map<string, string> = new Map()

        await locals.prisma.department.update({
            where: {
                id: employee.departmentId,
            },
            data: {
                timecardValidations: {
                    connectOrCreate: {
                        where: {
                            cutoffId_userId: {
                                cutoffId: params.id,
                                userId: bsUser.user_id
                            }
                        },
                        create: {
                            cutoffId: params.id,
                            userId: bsUser.user_id,
                            statusId: TimeCardStatus.Draft,
                            displayName: bsUser.name,
                            email: bsUser.email,
                            headId: employee.department?.head?.id,
                            headName: employee.department?.head?.name,
                        },
                    }
                }
            }
        });

        const timeCardValidation = await locals.prisma.timeCardValidation.findFirstOrThrow({
            where: {
                cutoffId: params.id,
                userId: bsUser.user_id,
            }
        });
        for (const row of timecard) {
            const dispute = form.get(`dispute[${row.id}]`);
            const remarks = form.get(`remarks[${row.id}]`);

            const payload = {
                inTimeString: row.in_time_string ?? '',
                outTimeString: row.out_time_string ?? '',
                shift: row.shift ?? '',
                exceptions: row.exceptions,
                leaves: row.leaves,
                holidays: row.holidays,
                userStatus: TimeCardUserStatus.Accepted,
                userRemarks: remarks?.toString() ?? '',
                overtimeFinal: row.normal.overtime ?? 0,
                unfulfilled: row.unfulfilled_work_time,
                unfulfilledFinal: isNaN(row.unfulfilledFinal) ? 0 : row.unfulfilledFinal,
                overBreak: row.break.over_break,
                overBreakFinal: isNaN(row.overBreakFinal) ? 0 : row.overBreakFinal,
                absenceFinal: isNaN(row.absenceFinal) ? 0 : row.absenceFinal,
                timeCardObject: row
            };
            const timeCardDetail = await locals.prisma.timeCardValidationDetail.upsert({
                where: {
                    timeCardValidationId_timeCardDateString: {
                        timeCardValidationId: timeCardValidation.id,
                        timeCardDateString: row.id
                    }
                },
                create: {
                    ...payload,
                    timeCardValidationId: timeCardValidation.id,
                    timeCardDateString: row.id,
                    // overBreakFinal: isNaN(row.overBreakFinal) ? 0 : row.overBreakFinal,
                },
                update: {
                    ...payload,
                    updatedAt: new Date()
                }
            });

            if (dispute === null) continue;
            if (typeof remarks === 'string' && remarks.trim().length === 0) errors.set(row.id, 'Remarks is required');
            await locals.prisma.timeCardValidationDetail.update({
                where: { id: timeCardDetail.id },
                data: {
                    userStatus: TimeCardUserStatus.Disputed
                }
            })
        }
        if (errors.size) {
            return fail(400, { errors, data: new Map(form.entries()) });
        }
        await locals.prisma.timeCardValidation.update({
            where: { id: timeCardValidation.id },
            data: {
                statusId: TimeCardStatus.EmployeeSubmitted,
                updatedAt: new Date()
            }
        });
        return {};
    }
} satisfies Actions;