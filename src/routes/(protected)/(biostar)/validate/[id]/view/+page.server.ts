import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals, params, parent }) => {
    const { bsUser } = await parent();
    const timecard = await locals.prisma.timeCardValidation.findFirst({
        where: {
            id: params.id,
            userId: bsUser.user_id
        },
        include: {
            timeCardValidationDetails: {
                orderBy: [
                    {
                        timeCardDateString: 'asc'
                    },
                    {
                        inTimeString: 'asc'
                    }
                ]
            }
        }
    });
    if (!timecard) throw error(404, 'Timecard not found');
    return {
        timecard: {
            ...timecard,
            totalUnfulfilledFinalHrd: timecard.totalUnfulfilledFinalHrd.toNumber(),
            totalOverBreakFinalHrd: timecard.totalOverBreakFinalHrd.toNumber(),
            totalOvertimeFinalHrd: timecard.totalOvertimeFinalHrd.toNumber(),
            totalAbsenceFinalHrd: timecard.totalAbsenceFinalHrd.toNumber(),
            timeCardValidationDetails: timecard.timeCardValidationDetails.map((t) => ({
                    ...t,
                    overtimeFinal: t.overtimeFinal.toNumber(),
                    overtimeFinalHrd: t.overtimeFinalHrd.toNumber(),
                    unfulfilled: t.unfulfilled.toNumber(),
                    overBreak: t.overBreak.toNumber(),
                    punchBreak: t.punchBreak.toNumber(),
                    unfulfilledFinal: t.unfulfilledFinal.toNumber(),
                    unfulfilledFinalHrd: t.unfulfilledFinalHrd.toNumber(),
                    overBreakFinal: t.overBreakFinal.toNumber(),
                    overBreakFinalHrd: t.overBreakFinalHrd.toNumber(),
                    absenceFinal: t.absenceFinal.toNumber(),
                    absenceFinalHrd: t.absenceFinalHrd.toNumber(),
                })
            )
        }
    };
}) satisfies PageServerLoad;