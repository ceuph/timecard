import { error } from '@sveltejs/kit';
import type { LayoutServerLoad } from './$types';

export const load = (async ({ locals, parent }) => {
    const { user } = await parent();
    const bsUser = await locals.biostar.getUserByEmail(user.username);
    if (!bsUser) {
        console.error('User not in Biostar', { user, bsUser });
        throw error(400, 'Your email is not associated to a biometrics account, please contact HRD for assistance.')
    };
    return { bsUser };
}) satisfies LayoutServerLoad;