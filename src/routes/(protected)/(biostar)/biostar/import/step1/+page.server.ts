import { fail, redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import type { Actions } from "./$types";
import os from 'os';
import path from 'path';
import { writeFile } from 'fs/promises';

export const load = (async () => {
    return {};
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ request }) => {
		const data = await request.formData();
		const file = data.get('file') as File;
		if (file.type !== 'text/csv') {
			return fail(400, { error: 'File must be of type text/csv' });
		}

		const timestamp = Math.floor(new Date().getTime() / 1000);
		const seed = Math.floor(Math.random() * 1000);
		const fileName = `${timestamp}-${seed}-${file.name}`;
		const filePath = path.join(os.tmpdir(), fileName);
		await writeFile(filePath, Buffer.from(await file.arrayBuffer()));
		throw redirect(303, `/hrd/import/step2?file=${encodeURIComponent(fileName)}`);
	}
} satisfies Actions;