import type { DataTableNonEmptyHeader, DataTableRow } from 'carbon-components-svelte/src/DataTable/DataTable.svelte';
import type { PageServerLoad } from './$types';
import fs from 'fs';
import os from 'os';
import path from 'path';
import csv from 'csv-parser';
import type { Actions } from "./$types";
import { error, redirect } from '@sveltejs/kit';
import BiostarResponseError from '$lib/server/biostar/BiostarResponseError';

export const load = (async ({ url }) => {
    const fileName = decodeURIComponent(url.searchParams.get('file')?.toString() ?? '').replace('..', '').replace('/', '');
    const filePath = path.join(os.tmpdir(), fileName);
    const limit = 5;
    const rows: Array<DataTableRow> = [];
    const headers: Array<DataTableNonEmptyHeader> = [];
    let count = 0;
    if (!fs.existsSync(filePath)) throw error(400, 'File does not exists.');

    await (new Promise<boolean>((resolve, reject) => {
        fs.createReadStream(filePath, { encoding: 'utf8' })
            .pipe(csv())
            .on('headers', (data) => {
                for (const header of data) {
                    headers.push({ key: header, value: header });
                }
            })
            .on('data', (line) => {
                if (count > limit) return;
                rows.push({ id: count++, ...line });
            })
            .on('end', () => resolve(true))
            .on('error', (err) => reject(err));
    }));
    return { fileName, rows, headers };
}) satisfies PageServerLoad;

export const actions = {
    default: async ({ request, url, locals }) => {
        const data = await request.formData();
        const empIdProp = data.get('empId')?.toString() ?? '';
        const emailProp = data.get('email')?.toString() ?? '';
        const fileName = decodeURIComponent(url.searchParams.get('file')?.toString() ?? '').replace('..', '').replace('/', '');
        const filePath = path.join(os.tmpdir(), fileName);
        const bgJob = await createJob(locals);
        processCsv(filePath, bgJob.id, empIdProp, emailProp, locals);
        redirect(303, `/biostar/import/step3/${bgJob.id}`);
    }
} satisfies Actions;

const processCsv = async (filePath: string, jobId: string, empIdProp: string, emailProp: string, locals: App.Locals) => {
    const lines: Array<Record<string, string>> = [];
    await (new Promise<boolean>((resolve, reject) => {
        fs.createReadStream(filePath, { encoding: 'utf8' })
            .pipe(csv())
            .on('data', (line) => lines.push(line))
            .on('end', () => resolve(true))
            .on('error', (err) => reject(err));
    }));
    for (const line of lines) {
        await handleData(jobId, empIdProp, emailProp, line, locals);
    }
    await closeJob(jobId, locals);
};

const createJob = async (locals: App.Locals) => {
    return await locals.prisma.backgroundJob.create({
        data: {
            type: 'emailMapping',
            status: 'running',
        }
    });
}

const closeJob = async (jobId: string, locals: App.Locals) => {
    await locals.prisma.backgroundJob.update({
        where: {
            id: jobId
        },
        data: {
            status: 'completed',
            updatedAt: new Date()
        }
    });
}
const handleData = async (jobId: string, empIdProp: string, emailProp: string, line: Record<string, string>, locals: App.Locals) => {
    try {
        const jsonResponse = await locals.biostar.updateUserEmail(line[empIdProp], line[emailProp]);
        await locals.prisma.backgroundJobLog.create({
            data: {
                backgroundJobId: jobId,
                log: `Success: ${JSON.stringify(jsonResponse)}`
            }
        });
    } catch (err) {
        if (err instanceof BiostarResponseError) {
            await locals.prisma.backgroundJobLog.create({
                data: {
                    backgroundJobId: jobId,
                    log: `${err.message} | ${err.status} | ${JSON.stringify(err.body)}`
                }
            });
        } else {
            console.error(err);
        }
    }
};