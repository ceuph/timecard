import type { PageServerLoad } from './$types';

export const load = (async ({ locals, parent }) => {
    const isHRD = locals.roles.has('HRD');
    const { user } = await parent();
    const timecards = await locals.prisma.timeCardValidation.findMany({
        where: {
            userId: !isHRD ? user.id : undefined,
        },
        include: {
            cutoff: true,
            department: true,
            head: true,
        }
    });
    return { 
        timecards: timecards.map(t => ({
            id: t.id,
            startDate: t.cutoff.startDate,
            endDate: t.cutoff.endDate,
            department: t.department.name,
            name: t.displayName,
            statusId: t.statusId,
        })) 
    };
}) satisfies PageServerLoad;