import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    let department;
	const isHRD = locals.roles.has('HRD');
	const isACCOUNTING = locals.roles.has('ACCOUNTING');
	if (locals.user?.username) {
		department = await locals.prisma.department.findFirst({
			where: { headId: locals.user.username }
		})
	}
	
    return { department, isHRD, isACCOUNTING };
}) satisfies PageServerLoad;