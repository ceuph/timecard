import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ locals }) => {
    const info = await locals.mail.sendMail({
        from: 'mailer@ceu.edu.ph', // sender address
        to: "jochua@ceu.edu.ph", // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
    });
    return json(info);
};