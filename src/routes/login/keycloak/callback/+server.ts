import type { RequestHandler } from './$types';
import { OAuth2RequestError } from 'arctic';
import { generateIdFromEntropySize } from 'lucia';
import { createKeycloakAuth, fetchKeycloakProfile } from '$lib/server/auth/keycloak';
import { error, redirect } from '@sveltejs/kit';
import { AuthorizationError } from '$lib/server/errors/AuthorizationError';

const validateUser = (email: string) => {
	if (!email.match(/ceu.edu.ph$/)) {
		throw new AuthorizationError('You are not authorized to access this page.');
	}
};

export const GET: RequestHandler = async ({ url, cookies, locals }) => {
	const keycloak = createKeycloakAuth(url);
	const code = url.searchParams.get('code');
	const state = url.searchParams.get('state');
	const storedState = cookies.get('keycloak_oauth_state') ?? null;
	const storedCodeVerifier = cookies.get('keycloak_oauth_code_verifier') ?? null;
	if (!code || !storedState || !storedCodeVerifier || state !== storedState) {
		throw error(400, 'Invalid request');
	}

	try {
		const tokens = await keycloak.validateAuthorizationCode(code, storedCodeVerifier);
		const response = await fetchKeycloakProfile(tokens);
		const json = await response.json();
		const email = typeof json.email === 'string' ? json.email : '';
		validateUser(email);
		const data = {
			username: email,
			name: typeof json.name === 'string' ? json.name : undefined,
			picture: typeof json.picture === 'string' ? json.picture : undefined
		};
		const user = await locals.prisma.user.upsert({
			where: { username: email },
			create: data,
			update: {
				...data,
				updatedAt: new Date()
			}
		});
		const session = await locals.lucia.createSession(user.id, {});
		const sessionCookie = locals.lucia.createSessionCookie(session.id);
		cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '.',
			...sessionCookie.attributes
		});
	} catch (e) {
		console.error(e);
		if (e instanceof OAuth2RequestError) {
			throw error(500, e.message);
		}
		throw error(500, `Unknow error.`);
	}
	redirect(303, '/');
};
