import type { RequestHandler } from './$types';
import { redirect } from '@sveltejs/kit';
import { generateCodeVerifier, generateState } from 'arctic';
import { createKeycloakAuth } from '$lib/server/auth/keycloak';

export const GET: RequestHandler = async ({ url, cookies }) => {
	const state = generateState();
	const codeVerifier = generateCodeVerifier();
	const keycloak = createKeycloakAuth(url);
	const authURL: URL = await keycloak.createAuthorizationURL(state, codeVerifier, {
		scopes: ['profile', 'email']
	});
	cookies.set('keycloak_oauth_state', state, {
		path: '/',
		secure: import.meta.env.PROD,
		httpOnly: true,
		maxAge: 60 * 10,
		sameSite: 'lax'
	});
	cookies.set('keycloak_oauth_code_verifier', codeVerifier, {
		path: '/',
		secure: import.meta.env.PROD,
		httpOnly: true,
		maxAge: 60 * 10,
		sameSite: 'lax'
	});
	throw redirect(303, authURL);
};
